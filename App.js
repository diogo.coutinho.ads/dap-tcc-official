import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import firebase from 'firebase';

import Router from './Router';

const customTextProps = {
  style: {
    fontFamily: 'roboto-regular'
  }
};

console.disableYellowBox = true;

class App extends Component {
  componentWillMount () {
    //Posso Fazer qualquer tipo de configuração global aqui como por exemplo o Firebase
    if (firebase.apps.length === 0) {
      firebase.initializeApp({
        apiKey: "AIzaSyAJioNRizMC8kwxfeLIdNWmE1l-zalaNWE",
        authDomain: "tcc-dashboard.firebaseio.com",
        databaseURL: "https://tcc-dashboard.firebaseio.com",
        projectId: "tcc-dashboard",
        storageBucket: "tcc-dashboard.firebaseio.com",
        messagingSenderId: "704372823193"
      })
    }
  //   SplashScreen.close({
  //     animationType: SplashScreen.animationType.scale,
  //     duration: 100,
  //     delay: 50,
  //  })
   }

  render() {
    return (
      <Router></Router>
    );
  }
}

export default App